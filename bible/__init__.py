import json
import re
from flask import Flask, render_template, request, flash

app = Flask(__name__)
app.secret_key = "dev"

book_abbreviations = {
    "Gen": "Genesis",
    "Ex": "Exodus",
    "Exo": "Exodus",
    "Exod": "Exodus",
    "Lev": "Leviticus",
    "Num": "Numbers",
    "Deu": "Deuteronomy",
    "Deut": "Deuteronomy",
    "Josh": "Joshua",
    "Judg": "Judges",
    "Ru": "Ruth",
    "1 Sam": "1 Samuel",
    "2 Sam": "2 Samuel",
    "1 Kgs": "1 Kings",
    "2 Kgs": "2 Kings",
    "1 Chr": "1 Chronicles",
    "2 Chr": "2 Chronicles",
    "Ez": "Ezra",
    "Neh": "Nehemiah",
    "Est": "Esther",
    "Jo": "Job",
    "Ps": "Psalms",
    "Psalm": "Psalms",
    "Prov": "Proverbs",
    "Ecc": "Ecclesiastes",
    "Eccl": "Ecclesiastes",
    "Song": "Song of Solomon",
    "Is": "Isaiah",
    "Isa": "Isaiah",
    "Jer": "Jeremiah",
    "Jere": "Jeremiah",
    "Lam": "Lamentations",
    "Ez": "Ezekiel",
    "Eze": "Ezekiel",
    "Ezek": "Ezekiel",
    "Dan": "Daniel",
    "Ho": "Hosea",
    "Hos": "Hosea",
    "Joe": "Joel",
    "Am": "Amos",
    "Amo": "Amos",
    "Ob": "Obadiah",
    "Oba": "Obadiah",
    "Obad": "Obadiah",
    "Jon": "Jonah",
    "Mi": "Micah",
    "Mic": "Micah",
    "Mica": "Micah",
    "Na": "Nahum",
    "Nah": "Nahum",
    "Ha": "Habakkuk",
    "Hab": "Habakkuk",
    "Zep": "Zephaniah",
    "Zeph": "Zephaniah",
    "Hag": "Haggai",
    "Hagg": "Haggai",
    "Zec": "Zechariah",
    "Zech": "Zechariah",
    "Mal": "Malachi",
    "Mala": "Malachi",
    "Mat": "Matthew",
    "Matt": "Matthew",
    "Mar": "Mark",
    "Lu": "Luke",
    "Luk": "Luke",
    "Jn": "John",
    "Ac": "Acts",
    "Act": "Acts",
    "Ro": "Romans",
    "Rom": "Romans",
    "Roma": "Romans",
    "1 Cor": "1 Corinthians",
    "2 Cor": "2 Corinthians",
    "Ga": "Galatians",
    "Gal": "Galatians",
    "Gala": "Galatians",
    "Galat": "Galatians",
    "Ep": "Ephesians",
    "Eph": "Ephesians",
    "Ephe": "Ephesians",
    "Ephes": "Ephesians",
    "Phi": "Philippians",
    "Phil": "Philippians",
    "Co": "Colossians",
    "Col": "Colossians",
    "Colo": "Colossians",
    "Colos": "Colossians",
    "Coloss": "Colossians",
    "1 Th": "1 Thessalonians",
    "1 The": "1 Thessalonians",
    "1 Thes": "1 Thessalonians",
    "1 Thess": "1 Thessalonians",
    "2 Th": "2 Thessalonians",
    "2 The": "2 Thessalonians",
    "2 Thes": "2 Thessalonians",
    "2 Thess": "2 Thessalonians",
    "1 Ti": "1 Timothy",
    "1 Tim": "1 Timothy",
    "1 Timo": "1 Timothy",
    "Ti": "Titus",
    "Tit": "Titus",
    "Titu": "Titus",
    "Ph": "Philemon",
    "Phi": "Philemon",
    "Phil": "Philemon",
    "Phile": "Philemon",
    "Philem": "Philemon",
    "He": "Hebrews",
    "Heb": "Hebrews",
    "Hebr": "Hebrews",
    "Hebre": "Hebrews",
    "Hebrew": "Hebrews",
    "Ja": "James",
    "Jam": "James",
    "Jame": "James",
    "1 Pe": "1 Peter",
    "1 Pet": "1 Peter",
    "1 Pete": "1 Peter",
    "2 Pe": "2 Peter",
    "2 Pet": "2 Peter",
    "2 Pete": "2 Peter",
    "1 Jo": "1 John",
    "1 Jn": "1 John",
    "2 Jo": "2 John",
    "2 Jn": "2 John",
    "3 Jo": "3 John",
    "3 Jn": "3 John",
    "Ju": "Jude",
    "Jud": "Jude",
    "Re": "Revelation",
    "Rev": "Revelation",
    "Reve": "Revelation",
    "Revel": "Revelation",
    "Revela": "Revelation",
    "Revelat": "Revelation"
}

VALID_REFERENCE_PATTERN = re.compile(r"([A-Za-z1-3 ]+) ([0-9-]+)(:[0-9-]+)?")

def extract_chapters(chapters_string):
    chapters = None
    if "-" in chapters_string:
        # parse chapters range e.g. 1-3
        chapters = list(map(int, chapters_string.split("-")))
        # range is exclusive, so we need to add 1 to the last chapter number
        chapters[1] = chapters[1] + 1
        chapters = list(range(chapters[0], chapters[1]))
    else:
        chapters = [int(chapters_string)]
    return chapters

def extract_verses(verses_string):
    verses = None
    if verses_string:
        # strip off the colon
        verses_string = verses_string[1:]
        if "-" in verses_string:
            # parse verses range e.g. 1-3
            verses = list(map(int, verses_string.split("-")))
            # range is exclusive, so we need to add 1 to the last verse number
            verses[1] = verses[1] + 1
            verses = list(range(verses[0], verses[1]))
        else:
            verses = [int(verses_string)]
    return verses

@app.route("/", methods=("GET", "POST"))
def home():
    if request.method == "POST":
        reference = request.form["reference"]
        error = None

        if not reference:
            error = "Reference is required."
        
        if error is not None:
            flash(error)
        else:
            reference_match = VALID_REFERENCE_PATTERN.match(reference.title())
            if not reference_match:
                flash("Invalid reference (try 'genesis 1' or 'genesis 1:4')")
            else:
                book = reference_match.group(1)
                book = book_abbreviations[book] if book in book_abbreviations else book
                chapters_string = reference_match.group(2)
                chapters = extract_chapters(chapters_string)
                verses_string = reference_match.group(3)
                verses = extract_verses(verses_string)

                found_verses = []
                with open("data/kjv.json") as biblef:
                    for line in biblef:
                        data = json.loads(line)
                        if verses:
                            if data["book_name"] == book and data["chapter"] in chapters and data["verse"] in verses:
                                found_verses.append(data)
                        else:
                            if data["book_name"] == book and data["chapter"] in chapters:
                                found_verses.append(data)
                return render_template("home.html", verses=found_verses, reference=reference, full_reference=book + " " + chapters_string)
    return render_template("home.html")
